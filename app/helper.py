import os
import sys
import json, datetime, time

from apscheduler.scheduler import Scheduler

from app import db, config
from app.logger import logger

from lib.converter import Converter
import subprocess
import json

c = Converter()


def needs_convert(filepath, queue):
    info = c.probe(filepath)
    logger.info(u"checking if '%s' needs conversion" % filepath)

    if "_UNPACK" in filepath:
        logger.info(u"foldername says _UNPACK... skipping")
        return False

    if "-CONVERTED-" in filepath:
        logger.info(u"file seems to be already converted... skipped")
        return False

    if not config.audio_map[info.audio.codec] or not config.audio_map[queue.profile.audio_codec]:
        logger.info(u"codec map not found...")
        logger.debug(u"source audio codec: '%s', video codec: '%s'" % (info.audio.codec, info.video.codec))
        logger.debug(u"target audio codec: '%s', video codec: '%s'" % (queue.profile.audio_codec, queue.profile.video_codec))
        return True

    if config.audio_map[info.audio.codec] > config.audio_map[queue.profile.audio_codec]:
        logger.info(u"audio codec should be converted from '%s' to '%s'" % (info.audio.codec, queue.profile.audio_codec))
        logger.debug(u"source audio codec score: '%s', target audio codec score: '%s'" % (config.audio_map[info.audio.codec], config.audio_map[queue.profile.audio_codec]) )
        return True

    if queue.profile.conv_video:
        if info.video.codec != queue.profile.video_codec and info.video.codec != "copy":
            logger.info(u"video codec should be converted from '%s' to '%s'" % (info.video.codec, queue.profile.video_codec))
            return True

        if queue.profile.video_codec == "copy":
            queue.video_codec == info.video.codec

        if info.video.bitrate > queue.profile.video_bitrate:
            logger.info(u"source bitrate is higher than target...")
            return True

    return False


def convert_from_queue(queue):
    logger.info(u"conversion started for: '%s' " % queue.path)
    if queue.profile.create_thumbnail:
        logger.debug(u"seems like i should make some thumbnails...")

        if queue.profile.thumbnail_count > 1:
            logger.info(u"generating %s thumbnails" % queue.profile.thumbnail_count)
            mythumbs = []
            cur_count = 0
            if not queue.profile.thumbnail_interval:
                thumbnail_interval = 60
            else:
                thumbnail_interval = queue.profile.thumbnail_interval

            cur_interval = 60
            while queue.profile.thumbnail_count > cur_count:
                thumbnail_path = "%s-%03d.jpg" % (os.path.splitext(queue.path)[0], cur_count)
                cur_thumb = ( cur_interval, thumbnail_path, None, 2 )
                mythumbs.append(cur_thumb)
                cur_interval += thumbnail_interval
                cur_count += 1

            logger.debug(u"thumbnail command: %s" % mythumbs)
            c.thumbnails(queue.path, mythumbs)

        else:
            thumbnail_path = "%s.jpg" % (os.path.splitext(queue.path)[0])
            logger.info(u"generating thumbnail %s" %thumbnail_path)
            c.thumbnail(queue.path, 60, thumbnail_path, None, 2)

    converted_file = "%s-CONVERTED-%s%s" % (os.path.splitext(queue.path)[0], queue.profile.name, os.path.splitext(queue.path)[1])
    logger.debug(u"filename for new converted file: %s" % converted_file)
    conv_string = json.loads(queue.conv_string)

    ## fix for copy video codec when other video settings where given...
    if conv_string["video"]["codec"] == "copy" and len(conv_string["video"]) > 1:
        info = c.probe(queue.path)
        conv_string["video"]["codec"] = info.video.codec

    logger.debug(u"convert string: %s" % conv_string)
    cur_conv = c.convert(queue.path, converted_file, conv_string)
    old_time = 0
    for curtime in cur_conv:
        if curtime != old_time:
            old_time = curtime
            queue.progress = curtime
            db.session.merge(queue)
            db.session.commit()
            logger.debug(u"converted %02d percent" % curtime)
    logger.info(u"conversion complete")

def converters():
    result = db.session.query(db.Queue).filter(db.Queue.progress == 0).all()
    for queue in result:
        convert_from_queue(queue)

def walk_dir(dirname):
    filelist = []
    for dirpath, dnames, fnames in os.walk(u"%s" % dirname):
        for f in fnames:
            if os.path.splitext(f)[1] in config.medialist:
                filelist.append(os.path.join(dirpath, f))
    return filelist

def is_media_file(filepath):
    if os.path.splitext(filepath)[1] in config.medialist:
        return True
    else:
        logger.warning(u"'%s' seems not to be a valid media file (%s)" % ( os.path.basename(filepath), os.path.splitext(filepath)[1] ))
        return False

def make_conv_string(myprofile):
    conv_string = {}

    conv_string['format'] = myprofile.container_format

    #hardcode this for now... copy subtitles!
    conv_string['subtitle'] = {
        'codec': 'copy'
    }

    if myprofile.conv_audio:
        conv_string['audio'] = {
            'bitrate': int(myprofile.audio_bitrate),
            'codec': myprofile.audio_codec,
            'channels': myprofile.audio_channels,
        }
    else:
        conv_string['audio'] = {
            'codec': 'copy'
        }

    if myprofile.conv_video:
        conv_string['video'] = {
            'codec': myprofile.video_codec
            # 'bitrate': myprofile.video_bitrate,
            # 'fps': myprofile.video_fps,
            # 'mode': str,
            # 'src_width': int,
            # 'src_height': int,
        }
        if myprofile.video_height:
            conv_string['video']['height'] = myprofile.video_height
        if myprofile.video_width:
            conv_string['video']['width'] = myprofile.video_width
        if myprofile.video_fps:
            conv_string['video']['fps'] = myprofile.video_fps
        if myprofile.video_bitrate:
            conv_string['video']['bitrate'] = myprofile.video_bitrate
    else:
        conv_string['video'] = {
            'codec': 'copy'
        }

    if myprofile.all_streams:
        conv_string['map'] = 0

    myprofile.conv_string = json.dumps(conv_string)
    db.session.merge(myprofile)
    db.session.commit()

def scan_files(folderpath):
    folder = db.session.query(db.Folders).filter(db.Folders.path == folderpath).first()
    logger.info(u"scanning folder %s" % folder.name)
    filelist = walk_dir(folder.path)
    for filepath in filelist:
        if is_media_file(filepath) and needs_convert(filepath, folder):
            result = db.session.query(db.Queue).filter(db.Queue.path == filepath).first()
            if not result:
                logger.info(u"adding '%s' to QUEUE" % filepath)
                ins = db.Queue(
                    path=filepath,
                    folder_id=folder.id,
                    filename=os.path.basename(filepath),
                    profile_id=folder.profile_id
                )
                ins.folder_name = folder.name
                ins.profile_name = folder.profile.name
                ins.conv_string = folder.profile.conv_string
                db.session.merge(ins)
                db.session.commit()
            else:
                logger.info(u"'%s' already in QUEUE" % filepath)

sched = Scheduler()
sched.start()

def start_sched():
    logger.debug(u"starting the sheduler")
    sched.add_interval_job(converters, minutes=30, max_instances=1, start_date=datetime.datetime.now() + datetime.timedelta(seconds=2))
    result = db.session.query(db.Folders).all()
    for curfolder in result:
        logger.debug(u"adding scan interval %s (minutes) for '%s'" % (curfolder.interval, curfolder.path))
        sched.add_interval_job(scan_files, name=curfolder.path, args=[curfolder.path], minutes=int(curfolder.interval), start_date=datetime.datetime.now() + datetime.timedelta(seconds=2))


def to_json(model):
    """ Returns a JSON representation of an SQLAlchemy-backed object.
    """
    myjson = {}
    # myjson['fields'] = {}
    # myjson['pk'] = getattr(model, 'id')

    for col in model._sa_class_manager.mapper.mapped_table.columns:
        myjson[col.name] = getattr(model, col.name)

    return myjson



def api_error(error):
    response = { "status": "error", "message": error }
    return response

def api_list_folders():
    result = db.session.query(db.Folders).all()
    response = { "status": "sucess", "message": "list of folders", "data": "" }
    mylist = []
    for entry in result:
        mylist.append(to_json(entry))

    response["data"] = mylist
    return response

def api_list_queue():
    result = db.session.query(db.Queue).all()
    response = { "status": "sucess", "message": "list of queue", "data": "" }
    mylist = []
    for entry in result:
        mylist.append(to_json(entry))

    response["data"] = mylist
    return response

def api_list_profiles():
    result = db.session.query(db.Profiles).all()
    response = { "status": "sucess", "message": "list of profiles", "data": "" }
    mylist = []
    for entry in result:
        mylist.append(to_json(entry))

    response["data"] = mylist
    return json.dumps(response)

def api_scan_folder(folder_id):
    result = db.session.query(db.Folders).filter(db.Folders.id == folder_id).first()
    exec_time = datetime.datetime.now() + datetime.timedelta(seconds=2)
    sched.add_date_job(scan_files, exec_time , [result.path])
    return json.dumps({ "status": "sucess", "message": "scanning folder %s" % result.name })

def api_start_queue():
    exec_time = datetime.datetime.now() + datetime.timedelta(seconds=2)
    sched.add_date_job(converters, exec_time)
    return json.dumps({ "status": "sucess", "message": "starting converting"})

def api_convert_job():
    exec_time = datetime.datetime.now() + datetime.timedelta(seconds=2)
    sched.add_date_job(converters, exec_time)
    return json.dumps({ "status": "sucess", "message": "converting xyz again."})
