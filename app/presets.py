atv3_720p = {
    'video': {
        'width': 1280,
        'heigh': 720,
        'codec': 'h264',
        'bitrate': 2500000
    },
    'audio': {
        'codec': 'ac3',
        'bitrate': 4410000,
        'channels': 6
    },
    'format': 'mp4'
}

#ffmpeg -i source_video.avi input -acodec aac -ab 128kb -vcodec mpeg4 -b 1200kb -mbd 2 -flags +4mv+trell -aic 2 -cmp 2 -subcmp 2 -s 320x180 -title X final_video.mp4
ios = {
    'video': {
        'width': 960,
        'heigh': 640,
        'codec': 'h264',
        'bitrate': 1200000
    },
    'audio': {
        'codec': 'aac',
        'bitrate': 160000,
        'channels': 2
    },
    'format': 'mp4'
}
#ffmpeg -i source_video.avi -b 300 -s 320x240 -vcodec xvid -ab 32 -ar 24000 -acodec aac final_video.mp4
psp = {
    'video': {
        'width': 320,
        'heigh': 240,
        'codec': 'xvid',
        'bitrate': 2500000
    },
    'audio': {
        'codec': 'aac',
        'bitrate': 32000,
        'channels': 2
    },
    'format': 'mp4'
}

wii = {
    'video': {
        'width': 640,
        'heigh': 480,
        'codec': 'pcm_u8',
    },
    'audio': {
        'codec': 'aac',
        'bitrate': 128000,
        'channels': 2
    },
    'format': 'avi'
}

xbox = {
    'video': {
        'width': 1280,
        'heigh': 720,
        'codec': 'h264',
        'bitrate': 2500000
    },
    'audio': {
        'codec': 'aac',
        'bitrate': 192000,
        'channels': 2
    },
    'format': 'mp4'
}

ps3 = {
    'video': {
        'width': 1280,
        'heigh': 720,
        'codec': 'h264',
        'bitrate': 2500000
    },
    'audio': {
        'codec': 'aac',
        'bitrate': 128000,
        'channels': 2
    },
    'format': 'mp4'
}
