from app import config
from app.logger import logger

from gittle import Gittle


def check_update():
    repo_path = config.basedir
    repo_url = 'https://raphaelmutschler@bitbucket.org/raphaelmutschler/pyencoder.git'

    repo = Gittle(repo_path)
    logger.info(u"Checking for updates...")
    if repo.active_sha != repo.head:
        config.commits_behind = repo.changes_count(repo.head, repo.active_sha)
        config.current_version = repo.active_sha
        config.latest_version = repo.head
        logger.info(u"you are %d commits behind, please update" %
                    config.commits_behind)
    else:
        logger.info(u"already up to date, no update needed")


def update():
    repo_path = config.basedir
    repo_url = 'https://raphaelmutschler@bitbucket.org/raphaelmutschler/pyencoder.git'

    repo = Gittle(repo_path)
    repo.pull()
