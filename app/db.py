from sqlalchemy import create_engine, MetaData, Column, Integer, String, or_, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, scoped_session, relationship
from app import config

from os import path

dbpath = path.join(config.datadir, "mediaconverter.db")
engine = create_engine('sqlite:///{0}'.format(dbpath), echo=False)

Base = declarative_base()

class Queue(Base):
    __tablename__ = 'queue'

    id = Column(Integer, primary_key=True, autoincrement=1)
    path = Column(String, unique=True)
    folder_name = Column(String)
    filename = Column(Integer)
    conv_string = Column(String)
    progress = Column(Integer, default=0)
    profile_id = Column(Integer, ForeignKey('profiles.id'))
    profile = relationship("Profiles")
    profile_name = Column(String)
    folder_id = Column(Integer, ForeignKey('folder.id'))
    folder = relationship("Folders")

    def __init__(self, path, folder_id, filename, profile_id):
        self.path = path
        self.folder_id = folder_id
        self.filename = filename
        self.profile_id = profile_id

    def __repr__(self):
        return u"<Queue('{0} {1})>".format(self.path, self.profile_id)


class Profiles(Base):
    __tablename__ = 'profiles'

    id = Column(Integer, autoincrement=1, primary_key=True)
    name = Column(String)
    conv_string = Column(String)
    conv_audio = Column(Integer)
    conv_video = Column(Integer)
    min_audio = Column(Integer)
    audio_disable = Column(Integer)
    audio_bitrate = Column(Integer)
    audio_codec = Column(String)
    audio_channels = Column(Integer)
    audio_samplerate = Column(Integer)
    video_codec = Column(String)
    video_bitrate = Column(Integer)
    video_fps = Column(Integer)
    video_width = Column(Integer)
    video_height = Column(Integer)
    video_mode = Column(String)
    video_src_width = Column(Integer)
    video_src_height = Column(Integer)
    video_disable = Column(Integer)
    container_format = Column(String)
    all_streams = Column(Integer)

    create_thumbnail = Column(Integer)
    thumbnail_count = Column(Integer)
    thumbnail_interval = Column(Integer)
    thumbnail_size = Column(String)
    thumbnail_quality = Column(Integer)

    def __init__(
            self, name, conv_audio, conv_video, container_format, create_thumbnail
        ):
        self.name = name
        self.conv_audio = conv_audio
        self.conv_video = conv_video
        self.container_format = container_format
        self.create_thumbnail = create_thumbnail

    def __repr__(self):
        return u"<Profile('{0} {1})>".format(self.id, self.name)

class Folders(Base):
    __tablename__ = 'folder'

    id = Column(Integer, autoincrement=1, primary_key=True)
    path = Column(String)
    name = Column(String)
    profile_id = Column(Integer, ForeignKey('profiles.id'))
    interval = Column(Integer)
    delete_finished = Column(Integer)
    move_finished = Column(Integer)
    move_to = Column(String)
    profile = relationship("Profiles")

    def __init__(self, path, name, interval, profile_id):
        self.path = path
        self.name = name
        self.interval = interval
        self.profile_id = profile_id

    def __repr__(self):
        return u"<Folder('{0} {1} {2})>".format(self.id, self.path, self.profile_id)



Base.metadata.create_all(engine)
Session = sessionmaker()
Session.configure(bind=engine)
session = scoped_session(Session)
# session = Session()

