from bottle import Bottle, run, template, TEMPLATE_PATH, static_file, request, redirect
from app import config, db, helper, git
import os
import datetime
from app.logger import logger

app = Bottle()

@app.route('/')
def hello():
    db.session.commit()
    result = db.session.query(db.Queue).all()
    return template('overview', content=result, topmenu="home", title="Home")

@app.route('/profiles')
def hello():
    result = db.session.query(db.Profiles).all()
    return template('profiles', content=result, topmenu="home", title="Profiles")

@app.route('/profile/edit/<profile_id:int>')
def edit_profile(profile_id):
    result = db.session.query(db.Profiles).filter(db.Profiles.id == profile_id).first()
    return template('profile', content=result, topmenu="home", title="Profiles")

@app.route('/folders')
def hello():
    result = db.session.query(db.Folders).all()
    profiles = db.session.query(db.Profiles).all()
    return template('folders', content=result, profiles=profiles, topmenu="home", title="Folders")

@app.route('/restart')
def restart():
    x = helper.restart()
    return x

@app.post('/folders/add')
def new_folder():
    path = request.forms.get('path')
    name = os.path.split(path)[1]
    p_id = request.forms.get('profile_id')
    interval = request.forms.get('interval')

    print p_id

    ins = db.Folders(
        path = path,
        name = name,
        interval = int(interval),
        profile_id = int(p_id)
        )
    print ins
    helper.sched.add_interval_job(helper.scan_files, name=path, args=[path], minutes=int(interval), start_date=datetime.datetime.now() + datetime.timedelta(seconds=2))
    print helper.sched.print_jobs()
    db.session.merge(ins)
    db.session.commit()
    return "Success"

@app.post('/profile/add')
def hello():
    print request.forms.__dict__

    audio_channels = int(request.forms.get('audio_channels'))
    settings = {}
    settings["conv_audio"] = False
    settings["conv_video"] = False
    settings["create_thumbnail"] = False
    settings["all_streams"] = False

    settings['video_codec'] = request.forms.get('video_codec')
    settings['video_height'] = request.forms.get('video_height')
    settings['video_width'] = request.forms.get('video_width')

    mylist = ["conv_audio", "conv_video", "create_thumbnail", "all_streams"]
    for x in mylist:
        if request.forms.get(x):
            settings[x] = True

    if request.forms.get('profile_id'):
        print "edit profile"
        insert = db.session.query(db.Profiles).filter(db.Profiles.id == request.forms.get('profile_id')).first()
        insert.name = request.forms.get("name")
        insert.conv_audio = settings["conv_audio"]
        insert.conv_video = settings["conv_video"]
        insert.container_format = request.forms.get('container_format')
        insert.create_thumbnail = settings["create_thumbnail"]
    else:
        print "new profile"
        insert = db.Profiles(
            name=request.forms.get("name"),
            conv_audio = settings["conv_audio"],
            conv_video = settings["conv_video"],
            container_format = request.forms.get('container_format'),
            create_thumbnail = settings["create_thumbnail"]
            )

    insert.audio_bitrate = int(request.forms.get('audio_bitrate'))
    insert.audio_channels = int(request.forms.get('audio_channels'))
    insert.audio_codec = request.forms.get('audio_codec')
    insert.all_streams = settings["all_streams"]
    insert.video_width = settings["video_width"]
    insert.video_height = settings["video_height"]
    insert.video_codec = settings["video_codec"]

    helper.make_conv_string(insert)

    # db.session.merge(insert)
    # db.session.commit()
    return "Success"
    # result = db.session.query(db.Profiles).filter(db.Profiles.id == id).all()
    # return template('profile-edit', content=result, topmenu="home", title="Home")


@app.post('/profile/delete')
def delete():
    todelete = request.forms.get("profile_id")
    result = db.session.query(db.Profiles).filter(db.Profiles.id == todelete).first()

    db.session.delete(result)
    db.session.commit()
    logger.info(u"deleted profile: '%s'" % result.name)
    return "Success"

@app.post('/folders/delete')
def delete():
    todelete = request.forms.get("folder_id")
    result = db.session.query(db.Folders).filter(db.Folders.id == todelete).first()
    #helper.sched.unschedule_job(helper.scan_files.job)
    alljobs = helper.sched.get_jobs()
    for job in alljobs:
        if job.name == result.path:
            helper.sched.unschedule_job(job)

    db.session.delete(result)
    db.session.commit()
    logger.info(u"deleted folder: '%s'" % result.name)
    return "Success"

@app.post('/queue/delete')
def delete():
    todelete = request.forms.get("queue_id")
    result = db.session.query(db.Queue).filter(db.Queue.id == todelete).first()
    db.session.delete(result)
    db.session.commit()
    logger.info(u"deleted item from queue: '%s'" % result.filename)
    return "Success"

@app.get('/update')
def check_update():
    git.check_update()
    #logger.info(u"checking for up")
    return git.check_update()

@app.route('/update/do')
def make_update():
    git.update()
    return "woop"

@app.route('/profile/<id>')
def hello(id):
    result = db.session.query(db.Profiles).filter(db.Profiles.id == id).all()
    return template('profile-edit', content=result, topmenu="home", title="Home")

@app.route('/static/<filepath:path>')
def server_static(filepath):
    return static_file(filepath, root=config.templatedir)


@app.route('/api')
def index():
    return template('api_options', content="api", topmenu="home", title="Home")

@app.route('/api/<apikey>/folders')
def list_folders(apikey):
    if apikey != config.apikey:
        return helper.api_error("wrong api key")

    return helper.api_list_folders()

@app.route('/api/<apikey>/profiles')
def list_folders(apikey):
    if apikey != config.apikey:
        return helper.api_error("wrong api key")

    return helper.api_list_profiles()

@app.route('/api/<apikey>/queue')
def list_folders(apikey):
    if apikey != config.apikey:
        return helper.api_error("wrong api key")

    return helper.api_list_queue()

@app.route('/api/<apikey>/queue/reset/<queue_id:int>')
def list_folders(apikey, queue_id):
    if apikey != config.apikey:
        return helper.api_error("wrong api key")

    result = db.session.query(db.Queue).filter(db.Queue.id == queue_id).first()
    result.progress = 0
    db.session.commit()
    return { "status": "sucess", "message": "resetting progress for %s" % result.filename }

@app.route('/api/<apikey>/scan/<folder:int>')
def index(apikey, folder):
    if apikey != config.apikey:
        return helper.api_error("wrong api key")

    return helper.api_scan_folder(folder)

@app.route('/api/<apikey>/convert')
def index(apikey):
    if apikey != config.apikey:
        return helper.api_error("wrong api key")

    return helper.api_start_queue()

def start_server():
    run(app, host='0.0.0.0', port=8085, debug=True)
