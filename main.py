import os
import sys
import signal

base_path = os.path.dirname(os.path.abspath(__file__))
import json
import datetime

# Insert local directories into path
sys.path.append(os.path.join(base_path, 'lib'))
from bottle import TEMPLATE_PATH
TEMPLATE_PATH.insert(0, os.path.join(base_path, "views"))

from app import helper
from app import web
from app import git, config
import time

# start the scheduler
helper.start_sched()
# add update checks to the sched
helper.sched.add_interval_job(git.check_update, hours=4, start_date=datetime.datetime.now() + datetime.timedelta(seconds=2))

# start up the webserver...
web.start_server()
