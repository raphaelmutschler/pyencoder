%include incl_top title=title, topmenu=topmenu
  <div class="container">
    <div class="page-header">
      <h1>Converter Profiles <small>this are profiles you have already created</small></h1>
    </div>

    <div class="col-sm-12 clearfix" style="margin-bottom: 10px">
      <div id="new_profile" class="btn btn-success pull-right">Add New</div>
    </div>
    <div id="create_profile" class="not-shown" style="display:none;">
      <div class="well">
        <form class="bs-example form-horizontal" id="myform" method="POST">
          <fieldset>
            <legend>Profile Settings</legend>
            <div class="form-group">
              <label for="name" class="col-lg-2 control-label">Name</label>
              <div class="col-lg-10">
                <input type="text" class="form-control" name="name" id="name" required placeholder="Name">
              </div>
            </div>

            <div class="form-group">
              <label for="container_format" class="col-lg-2 control-label">Container Format</label>
              <div class="col-lg-10">
                 <select class="form-control" id="container_format" name="container_format">
                  <!-- <option value="copy">copy</option> -->
                  <option value="mkv">mkv</option>
                  <option value="mp4">mp4</option>
                  <option value="avi">avi</option>
                  <option value="mov">mov</option>
                  <option value="ogg">ogg</option>
                  <option value="webm">webm</option>
                  <option value="flv">flv</option>
                  <option value="mpg">mpg</option>
                </select>
              </div>
            </div>

            <div class="form-group">
              <div class="col-lg-offset-2 col-lg-10">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="create_thumbnail" id="create_thumbnail" checked> Create thumbnail
                  </label>
                </div>
              </div>
            </div>

            <div class="thumbnail-settings">
            <div class="form-group">
              <label for="thumbnail_count" class="col-lg-2 control-label">How many thumbnails should i generate</label>
              <div class="col-lg-10">
                <input type="text" class="form-control" id="thumbnail_count" name="thumbnail_count" placeholder="1">
              </div>
            </div>
            <div class="form-group">
              <label for="thumbnail_interval" class="col-lg-2 control-label">Interval for thumbnails in seconds</label>
              <div class="col-lg-10">
                <input type="text" class="form-control" id="thumbnail_interval" name="thumbnail_interval" placeholder="120">
              </div>
            </div>
            <div class="form-group">
              <label for="thumbnail_size" class="col-lg-2 control-label">Size of the generated thumbnails</label>
              <div class="col-lg-10">
                <input type="text" class="form-control" id="thumbnail_size" name="thumbnail_size" placeholder="1280x720">
              </div>
            </div>
            <div class="form-group">
              <label for="thumbnail_quality" class="col-lg-2 control-label">Quality of the generated thumbnails</label>
              <div class="col-lg-10">
                <input type="text" class="form-control" id="thumbnail_quality" name="thumbnail_quality" placeholder="90">
              </div>
            </div>

            </div>

            <div class="form-group">
              <div class="col-lg-offset-2 col-lg-10">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="conv_audio" id="conv_audio" checked> Convert Audio
                  </label>
                </div>
              </div>
            </div>

            <div class="audio-settings">
            <div class="form-group">
              <label for="audio_codec" class="col-lg-2 control-label">Audio codec</label>
              <div class="col-lg-10">
                 <select class="form-control" id="audio_codec" name="audio_codec">
                  <option value="dts">dts</option>
                  <option value="ac3">ac3</option>
                  <option value="aac">aac</option>
                  <option value="libfdk_aac">libfdk_aac</option>
                  <option value="mp3">mp3</option>
                  <option value="vorbis">vorbis</option>
                  <option value="theora">theora</option>
                  <option value="mp2">mp2<option>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label for="audio_bitrate" class="col-lg-2 control-label">Audio Bitrate</label>
              <div class="col-lg-10">
                <input type="text" class="form-control" id="audio_bitrate" name="audio_bitrate" placeholder="448000">
              </div>
            </div>

            <div class="form-group">
              <label for="audio_channels" class="col-lg-2 control-label">Audio Channels</label>
              <div class="col-lg-10">
                <input type="text" class="form-control" id="audio_channels" name="audio_channels" placeholder="6">
              </div>
            </div>
            </div>

            <div class="form-group">
              <div class="col-lg-offset-2 col-lg-10">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="conv_video" id="conv_video"> Convert Video
                  </label>
                </div>
              </div>
            </div>

            <div class="video-settings" style="display:none">
            <div class="form-group">
              <label for="video_codec" class="col-lg-2 control-label">Video codec</label>
              <div class="col-lg-10">
                 <select class="form-control" id="video_codec" name="video_codec">
                  <option value="copy">copy</option>
                  <option value="h264">h264</option>
                  <option value="divx">divx</option>
                  <option value="vp8">vp8</option>
                  <option value="h263">h263</option>
                  <option value="flv">flv</option>
                  <option value="mpeg1">mpeg1</option>
                  <option value="mpeg2">mpeg2</option>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label for="video_width" class="col-lg-2 control-label">Video Width</label>
              <div class="col-lg-10">
                <input type="text" class="form-control" id="video_width" name="video_width" placeholder="1280">
              </div>
            </div>

            <div class="form-group">
              <label for="video_height" class="col-lg-2 control-label">Video Height</label>
              <div class="col-lg-10">
                <input type="text" class="form-control" id="video_height" name="video_height" placeholder="720">
              </div>
            </div>

            <div class="form-group">
              <label for="video_bitrate" class="col-lg-2 control-label">Video Bitrate</label>
              <div class="col-lg-10">
                <input type="text" class="form-control" id="video_bitrate" name="video_bitrate" placeholder="250000">
              </div>
            </div>

            <div class="form-group">
              <label for="video_fps" class="col-lg-2 control-label">Video FPS</label>
              <div class="col-lg-10">
                <input type="text" class="form-control" id="video_fps" name="video_fps" placeholder="25">
              </div>
            </div>
            </div>

            <div class="form-group">
              <div class="col-lg-offset-2 col-lg-10">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="all_streams" id="all_streams" checked> Map all streams to target File
                  </label>
                </div>
              </div>
            </div>

            <div class="form-group">
              <div class="col-lg-10 col-lg-offset-2">
                <button class="btn btn-default">Cancel</button>
                <button id="submit" class="btn btn-success">Submit</button>
              </div>
            </div>

          </fieldset>
        </form>
      </div>
    </div>
    %if content:
    <div class="table-responsive">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Audio</th>
            <th>Video</th>
            <th>Thumbs</th>
            <th>Options</th>
          </tr>
        </thead>
        %for profile in content:
        <tr data-profile-id="{{profile.id}}">
          <td>{{profile.id}}</td>
          <td>{{profile.name}}</td>
          <td>
            %if profile.conv_audio:
              <span class="text-success glyphicon glyphicon-ok"></span>
            %else:
              <span class="text-danger glyphicon glyphicon-remove"></span>
            %end if
          </td>
          <td>
            %if profile.conv_video:
              <span class="text-success glyphicon glyphicon-ok"></span>
            %else:
              <span class="text-danger glyphicon glyphicon-remove"></span>
            %end if
          </td>
          <td>
            %if profile.create_thumbnail:
              <span class="text-success glyphicon glyphicon-ok"></span>
            %else:
              <span class="text-danger glyphicon glyphicon-remove"></span>
            %end if
          </td>
          <td>
            <div class="btn btn-primary btn-sm"><a href="/profile/edit/{{profile.id}}"><span class="glyphicon glyphicon-pencil"></span> edit</a></div>
            <div class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-trash"></span> delete</div>
          </td>
        </tr>
        %end for
      </table>
    </div>
    %end if
  </div>

  <footer>

  </footer>


  <!-- JavaScript at the bottom for fast page loading -->

  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="/static/js/vendor/jquery-1.10.1.min.js"><\/script>')</script>

  <!-- scripts concatenated and minified via build script -->
  <script src="/static/js/vendor/bootstrap.min.js"></script>
  <script type="text/javascript" src="/static/js/jquery.validate.min.js"></script>
  <script type="text/javascript" src="/static/js/jquery.form.js"></script>
  <script src="/static/js/main.js"></script>
  <script type="text/javascript">


    $('#new_profile').click(function(){
      $('#create_profile').slideDown('fast', function() {
        $(this).removeClass('not-shown')
      });
    })

    $('#submit').click(function(event){
      $("#myform").validate({
        //'debug': true,
        'errorClass': 'has-error',
        'errorElement': 'em',
        highlight: function(element, errorClass) {
          $(element).parents('.form-group').addClass(errorClass);
        },
        rules: {
          audio_bitrate: {
            digits: true
          },
          audio_channels: {
            digits: true
          },
          video_bitrate: {
            digits: true
          },
          video_fps: {
            digits: true
          },
          video_width: {
            digits: true
          },
          video_height: {
            digits: true
          }
        },
        submitHandler: function(form) {
          $(form).ajaxSubmit({
            'url':'/profile/add',
            success: function(){
              location.reload()
            }
          });
          //console.log(form)
        }
      });
    })

    $('.btn-danger').click(function(event){
      todelete = $(this).parents('tr').data('profile-id');
      $.post("/profile/delete", {"profile_id": todelete}, function(data){
            console.log(data)

      })
      $(this).parents('tr').fadeOut()
    })

    $('#conv_video').on('change', function(){
      $('.video-settings').slideToggle();
    })

    $('#conv_audio').on('change', function(){
      $('.audio-settings').slideToggle();
    })

    $('#create_thumbnail').on('change', function(){
      $('.thumbnail-settings').slideToggle();
    })
  </script>
  <!-- end scripts -->


</body>
</html>



