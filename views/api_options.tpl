%include incl_top title=title, topmenu=topmenu
%from app import config
  <div class="container">
    <div class="page-header">
      <h1>Mediaconverter API <small>simple api documentation</small></h1>
    </div>

    <div class="well">
      Your Api Key: <code>{{config.apikey}}</code>
    </div>


<h3 id="listfolder">List folders</h3>
<p>URL: <strong><a href="/api/{{config.apikey}}/folders">/api/{{config.apikey}}/folders</a></strong></p>
<p>Response: json object of your folders </p>
<pre>{
  "status": "sucess",
  "message": "list of folders",
  "data": [
  {
    "delete_finished": null,
    "interval": 60,
    "name": "The.Ring.Thing.2004.German.AC3.DL.DVDRiP.x264",
    "profile_id": 1,
    "move_to": null,
    "path": "/Volumes/MacHD/The.Ring.Thing.2004.German.AC3.DL.DVDRiP.x264",
    "id": 1,
    "move_finished": null
  },
  {
    "delete_finished": null,
    "interval": 60,
    "name": "xyz",
    "profile_id": 1,
    "move_to": null,
    "path": "/xyz",
    "id": 2,
    "move_finished": null
  }]
}
</pre>


<h3 id="listqueue">List Queue</h3>
<p>URL: <strong><a href="/api/{{config.apikey}}/queue">/api/{{config.apikey}}/queue</a></strong></p>
<p>Response: json object of your queue </p>
<pre>{
  "status": "sucess",
  "message": "list of queue",
  "data": [
  {
    "profile_id": 1,
    "filename": "The.Ring.Thing.2004.German.AC3.DL.DVDRiP.x264.mkv",
    "profile_name": "DTS to AC3",
    "folder_name": "The.Ring.Thing.2004.German.AC3.DL.DVDRiP.x264",
    "progress": 4,
    "path": "/Volumes/MacHD/The.Ring.Thing.2004.German.AC3.DL.DVDRiP.x264/The.Ring.Thing.2004.German.AC3.DL.DVDRiP.x264.mkv",
    "id": 1,
    "folder_id": 1
  }]
}
</pre>


<h3 id="listprofiles">List Profiles</h3>
<p>URL: <strong><a href="/api/{{config.apikey}}/profiles">/api/{{config.apikey}}/profiles</a></strong></p>
<p>Response: json object of your profiles </p>
<pre>{
  "status": "sucess",
  "message": "list of profiles",
  "data": [
  {
    "audio_samplerate": null,
    "container_format": "mkv",
    "all_streams": 1,
    "audio_codec": "mp3",
    "conv_audio": 1,
    "video_src_height": null,
    "id": 1,
    "thumbnail_size": null,
    "video_src_width": null,
    "video_disable": null,
    "thumbnail_interval": null,
    "video_height": null,
    "conv_video": 0,
    "audio_channels": 2,
    "thumbnail_count": null,
    "audio_bitrate": 448000,
    "audio_disable": null,
    "video_width": null, "
    thumbnail_quality": null,
    "name": "DTS to AC3",
    "video_mode": null, "
    video_bitrate": null,
    "create_thumbnail": 0,
    "min_audio": null,
    "video_fps": null,
    "video_codec": null
  }]
}
</pre>

<h3 id="scanfolder">List Scan a folder</h3>
<p>URL: <strong><a href="/api/{{config.apikey}}/scan/FOLDER_ID">/api/{{config.apikey}}/scan/FOLDER_ID</a></strong></p>
<p>FOLDER_ID can be obtained by <a href="#listfolder">List Folders</a></p>
<p>Response: a status message... </p>
<pre>{
  "status": "sucess",
  "message": "scanning folder The.Ring.Thing.2004.German.AC3.DL.DVDRiP.x264"
}
</pre>


<h3 id="startqueue">Start Queue Converter</h3>
<p>URL: <strong><a href="/api/{{config.apikey}}/convert">/api/{{config.apikey}}/convert</a></strong></p>
<p>Response: a status message... </p>
<pre>{
  "status": "sucess",
  "message": "starting converting"
}
</pre>



  </div>

  <footer>

  </footer>


  <!-- JavaScript at the bottom for fast page loading -->

  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.1.min.js"><\/script>')</script>

  <!-- scripts concatenated and minified via build script -->
  <script src="js/vendor/bootstrap.min.js"></script>

  <script src="js/main.js"></script>
  <script type="text/javascript">

  </script>
  <!-- end scripts -->


</body>
</html>



