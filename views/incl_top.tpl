<!doctype html>
%from app import db, config
<html class="no-js" lang="de">
<head>
  <meta charset="utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>{{title}}</title>
  <meta name="description" content="">

  <meta name="viewport" content="width=device-width">

  <link rel="stylesheet" href="/static/css/bootstrap.min.css">
  <link rel="stylesheet" href="/static/css/notification.css">
  <link rel="stylesheet" href="/static/css/main.css">


</head>
<body>

  <header>
    <div class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <a href="../" class="navbar-brand">mediaconverter</a>
          <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="navbar-collapse collapse" id="navbar-main">
          <ul class="nav navbar-nav">
            <li>
              <a href="/profiles">Profiles</a>
            </li>
            <li>
              <a href="/folders">Folders</a>
            </li>
            <li>
              <a href="/api">API Docs</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </header>
  %if config.commits_behind > 1:
  <div id="update" class="container">
      <p class="alert alert-info">there is a <a href="http://bitbucket.org/raphaelmutschler/pyencoder/compare/{{config.current_version}}...{{config.latest_version}}" target="_blank">newer version available</a> (you are <b>{{config.commits_behind}} commits behind</b>) - <a href="/update/do">update now</a></p>
  </div>
  %end if
  <div id="back_to_top" style="border-radius: 10px; width: 40px; height: 40px; position:fixed; right: 40px; bottom:40px; background-color: #000; z-index: 10">
    <a href="#">back to top</a>
  </div>

