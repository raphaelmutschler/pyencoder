%include incl_top title=title, topmenu=topmenu
%import os
%from app import config
  <div class="container">
    <div class="page-header">
      <h1>Welcome to Mediaconverter <small>this is a simple webinterface</small></h1>
    </div>

    <div id="force-queue" class="btn btn-sm btn-success pull-right" style="margin-bottom: 10px"><span class="glyphicon glyphicon-play"></span> force queue</div>
    <div class="table-responsive">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>#</th>
            <th>Folder</th>
            <th>Filename</th>
            <th>Progress</th>
            <th>Profile</th>
            <th>options</th>
          </tr>
        </thead>
        %for queue in content:
        <tr data-queue-id="{{queue.id}}">
          <td>{{queue.id}}</td>
          <td>{{queue.folder_name}}</td>
          <td>{{queue.filename}}</td>
          <td><div class="progress" style="margin-bottom: 9px;">
                <div class="progress-bar progress-bar-success" style="line-height: 20px; width: {{queue.progress}}%">{{queue.progress}}%</div>
              </div></td>
          <td data-profile-id="{{queue.profile_id}}">{{queue.profile_name}}</td>
          <td>
            <span class="glyphicon glyphicon-refresh"></span>
            <div class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-trash"></span> delete</div>
          </td>
        </tr>
        %end for
      </table>
    </div>
  </div>

  <footer>

  </footer>


  <!-- JavaScript at the bottom for fast page loading -->

  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="/static/js/vendor/jquery-1.10.1.min.js"><\/script>')</script>

  <!-- scripts concatenated and minified via build script -->
  <script src="/static/js/vendor/bootstrap.min.js"></script>
  <script src="/static/js/notification.js"></script>
  <script src="/static/js/main.js"></script>
  <script type="text/javascript">
    $('.btn-danger').click(function(){
      var tr = $(this).parents('tr');
      console.log(tr.data("queue-id"))
      $.post('/queue/delete', {'queue_id': tr.data('queue-id')}, function(data){
        tr.fadeOut()
      })
    })

    $('#force-queue').click(function(){
      $.get('/api/{{config.apikey}}/convert', function(data){
        x = JSON.parse(data);
        if (x.status == "sucess") {
          successNotification("Sucess", x.message)
        }
      })
    })

    $('.glyphicon-refresh').click(function(){
      var tr = $(this).parents('tr');
      $.get('/api/{{config.apikey}}/queue/reset/' + tr.data('queue-id'), function(data){
        console.log(data)
        if (data.status == "sucess") {
          successNotification("Sucess", data.message)
        }
      })
    })
  </script>
  <!-- end scripts -->


</body>
</html>



