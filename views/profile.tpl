%include incl_top title=title, topmenu=topmenu
%import os
  <div class="container">
    <div class="page-header">
      <h1>Welcome to Mediaconverter <small>this is a simple webinterface</small></h1>
    </div>

    %if content:
    <form class="bs-example form-horizontal" id="myform" method="POST">
          <fieldset>
            <legend>Profile Settings for "{{content.name}}"</legend>
            <div class="form-group">
              <label for="name" class="col-lg-2 control-label">Name</label>
              <div class="col-lg-10">
                <input type="text" class="form-control" name="name" id="name" required value="{{content.name}}" placeholder="Name">
              </div>
            </div>

            <div class="hidden">
              <input type="text" class="form-control" id="profile_id" name="profile_id" value="{{content.id}}">
            </div>
            <div class="form-group">
              <label for="container_format" class="col-lg-2 control-label">Container Format</label>
              <div class="col-lg-10">
                 <select class="form-control" id="container_format" name="container_format">
                  <!-- <option value="copy">copy</option> -->
                  <option value="mkv"
                  %if content.container_format == "mkv":
                    selected
                  %end if
                  >mkv</option>
                  <option value="mp4"
                  %if content.container_format == "mp4":
                    selected
                  %end if
                  >mp4</option>
                  <option value="avi"
                  %if content.container_format == "avi":
                    selected
                  %end if
                  >avi</option>
                  <option value="mov"
                  %if content.container_format == "mov":
                    selected
                  %end if
                  >mov</option>
                  <option value="ogg"
                  %if content.container_format == "ogg":
                    selected
                  %end if
                  >ogg</option>
                  <option value="webm"
                  %if content.container_format == "webm":
                    selected
                  %end if
                  >webm</option>
                  <option value="flv"
                  %if content.container_format == "flv":
                    selected
                  %end if
                  >flv</option>
                  <option value="mpg"
                  %if content.container_format == "mpg":
                    selected
                  %end if
                  >mpg</option>
                </select>
              </div>
            </div>

            <div class="form-group">
              <div class="col-lg-offset-2 col-lg-10">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="create_thumbnail" id="create_thumbnail"
                    %if content.create_thumbnail == 1:
                      checked
                    %end if
                    > Create thumbnail
                  </label>
                </div>
              </div>
            </div>

            <div class="thumbnail-settings">
            <div class="form-group">
              <label for="thumbnail_count" class="col-lg-2 control-label">How many thumbnails should i generate</label>
              <div class="col-lg-10">
                <input type="text" class="form-control" id="thumbnail_count" name="thumbnail_count"\\
                %if content.thumbnail_count:
                  value="{{content.thumbnail_count}}"
                %end if
                placeholder="1">
              </div>
            </div>
            <div class="form-group">
              <label for="thumbnail_interval" class="col-lg-2 control-label">Interval for thumbnails in seconds</label>
              <div class="col-lg-10">
                <input type="text" class="form-control" id="thumbnail_interval" name="thumbnail_interval"\\
                %if content.thumbnail_interval:
                  value="{{content.thumbnail_interval}}"
                %end if
                placeholder="120">
              </div>
            </div>
            <div class="form-group">
              <label for="thumbnail_size" class="col-lg-2 control-label">Size of the generated thumbnails</label>
              <div class="col-lg-10">
                <input type="text" class="form-control" id="thumbnail_size" name="thumbnail_size"\\
                %if content.thumbnail_size:
                  value="{{content.thumbnail_size}}"
                %end if
                placeholder="1280x720">
              </div>
            </div>
            <div class="form-group">
              <label for="thumbnail_quality" class="col-lg-2 control-label">Quality of the generated thumbnails</label>
              <div class="col-lg-10">
                <input type="text" class="form-control" id="thumbnail_quality" name="thumbnail_quality"\\
                %if content.thumbnail_quality:
                  value="{{content.thumbnail_quality}}"
                %end if
                placeholder="90">
              </div>
            </div>

            </div>

            <div class="form-group">
              <div class="col-lg-offset-2 col-lg-10">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="conv_audio" id="conv_audio"
                    %if content.conv_audio:
                      checked
                    %end if
                    > Convert Audio
                  </label>
                </div>
              </div>
            </div>

            <div class="audio-settings">
            <div class="form-group">
              <label for="audio_codec" class="col-lg-2 control-label">Audio codec</label>
              <div class="col-lg-10">
                 <select class="form-control" id="audio_codec" name="audio_codec">
                  <option value="dts"
                  %if content.audio_codec == "dts":
                    selected
                  %end if
                  >dts</option>
                  <option value="ac3"
                  %if content.audio_codec == "ac3":
                    selected
                  %end if
                  >ac3</option>
                  <option value="aac"
                  %if content.audio_codec == "aac":
                    selected
                  %end if
                  >aac</option>
                  <option value="libfdk_aac"
                  %if content.audio_codec == "libfdk_aac":
                    selected
                  %end if
                  >libfdk_aac</option>
                  <option value="mp3"
                  %if content.audio_codec == "mp3":
                    selected
                  %end if
                  >mp3</option>
                  <option value="vorbis"
                  %if content.audio_codec == "vorbis":
                    selected
                  %end if
                  >vorbis</option>
                  <option value="theora"
                  %if content.audio_codec == "theora":
                    selected
                  %end if
                  >theora</option>
                  <option value="mp2"
                  %if content.audio_codec == "mp2":
                    selected
                  %end if
                  >mp2<option>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label for="audio_bitrate" class="col-lg-2 control-label">Audio Bitrate</label>
              <div class="col-lg-10">
                <input type="text" class="form-control" id="audio_bitrate" name="audio_bitrate"\\
                %if content.audio_bitrate:
                  value="{{content.audio_bitrate}}"\\
                %end if
                placeholder="448000">
              </div>
            </div>

            <div class="form-group">
              <label for="audio_channels" class="col-lg-2 control-label">Audio Channels</label>
              <div class="col-lg-10">
                <input type="text" class="form-control" id="audio_channels" name="audio_channels"\\
                %if content.audio_channels:
                  value="{{content.audio_channels}}"\\
                %end if
                placeholder="6">
              </div>
            </div>
            </div>

            <div class="form-group">
              <div class="col-lg-offset-2 col-lg-10">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="conv_video" id="conv_video"
                    %if content.conv_video:
                      checked\\
                    %end if
                    > Convert Video
                  </label>
                </div>
              </div>
            </div>

            <div class="video-settings">
            <div class="form-group">
              <label for="video_codec" class="col-lg-2 control-label">Video codec</label>
              <div class="col-lg-10">
                 <select class="form-control" id="video_codec" name="video_codec">
                  <option value="copy"
                  %if content.video_codec == "copy":
                    selected
                  %end if
                  >copy</option>
                  <option value="h264"
                  %if content.video_codec == "h264":
                    selected
                  %end if
                  >h264</option>
                  <option value="divx"
                  %if content.video_codec == "divx":
                    selected
                  %end if
                  >divx</option>
                  <option value="vp8"
                  %if content.video_codec == "vp8":
                    selected
                  %end if
                  >vp8</option>
                  <option value="h263"
                  %if content.video_codec == "h263":
                    selected
                  %end if
                  >h263</option>
                  <option value="flv"
                  %if content.video_codec == "flv":
                    selected
                  %end if
                  >flv</option>
                  <option value="mpeg1"
                  %if content.video_codec == "mpeg1":
                    selected
                  %end if
                  >mpeg1</option>
                  <option value="mpeg2"
                  %if content.video_codec == "mpeg2":
                    selected
                  %end if
                  >mpeg2</option>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label for="video_width" class="col-lg-2 control-label">Video Width</label>
              <div class="col-lg-10">
                <input type="text" class="form-control" id="video_width" name="video_width"
                %if content.video_width:
                  value="{{content.video_width}}"\\
                %end if
                placeholder="1280">
              </div>
            </div>

            <div class="form-group">
              <label for="video_height" class="col-lg-2 control-label">Video Height</label>
              <div class="col-lg-10">
                <input type="text" class="form-control" id="video_height" name="video_height"
                %if content.video_height:
                  value="{{content.video_height}}"\\
                %end if
                placeholder="720">
              </div>
            </div>

            <div class="form-group">
              <label for="video_bitrate" class="col-lg-2 control-label">Video Bitrate</label>
              <div class="col-lg-10">
                <input type="text" class="form-control" id="video_bitrate" name="video_bitrate"
                %if content.video_bitrate:
                  value="{{content.video_bitrate}}"\\
                %end if
                placeholder="250000">
              </div>
            </div>

            <div class="form-group">
              <label for="video_fps" class="col-lg-2 control-label">Video FPS</label>
              <div class="col-lg-10">
                <input type="text" class="form-control" id="video_fps" name="video_fps"
                %if content.video_fps:
                  value="{{content.video_fps}}"\\
                %end if
                placeholder="25">
              </div>
            </div>
            </div>

            <div class="form-group">
              <div class="col-lg-offset-2 col-lg-10">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="all_streams" id="all_streams"
                    %if content.all_streams:
                      checked\\
                    %end if
                    > Map all streams to target File
                  </label>
                </div>
              </div>
            </div>

            <div class="form-group">
              <div class="col-lg-10 col-lg-offset-2">
                <button id="submit" class="btn btn-success">Submit</button>
              </div>
            </div>

          </fieldset>
        </form>
    %end if
  </div>

  <footer>

  </footer>


  <!-- JavaScript at the bottom for fast page loading -->

  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="/static/js/vendor/jquery-1.10.1.min.js"><\/script>')</script>

  <!-- scripts concatenated and minified via build script -->
  <script src="/static/js/vendor/bootstrap.min.js"></script>
  <script type="text/javascript" src="/static/js/jquery.validate.min.js"></script>
  <script type="text/javascript" src="/static/js/jquery.form.js"></script>
  <script src="/static/js/main.js"></script>
  <script type="text/javascript">


    $('#new_profile').click(function(){
      $('#create_profile').slideDown('fast', function() {
        $(this).removeClass('not-shown')
      });
    })

    $('#submit').click(function(event){
      $("#myform").validate({
        //'debug': true,
        'errorClass': 'has-error',
        'errorElement': 'em',
        highlight: function(element, errorClass) {
          $(element).parents('.form-group').addClass(errorClass);
        },
        rules: {
          audio_bitrate: {
            digits: true
          },
          audio_channels: {
            digits: true
          },
          video_bitrate: {
            digits: true
          },
          video_fps: {
            digits: true
          },
          video_width: {
            digits: true
          },
          video_height: {
            digits: true
          }
        },
        submitHandler: function(form) {
          $(form).ajaxSubmit({
            'url':'/profile/add',
            success: function(){
              location.reload()
            }
          });
          //console.log(form)
        }
      });
    })

    $('.btn-danger').click(function(event){
      todelete = $(this).parents('tr').data('profile-id');
      $.post("/profile/delete", {"profile_id": todelete}, function(data){
            console.log(data)

      })
      $(this).parents('tr').fadeOut()
    })

    $('#conv_video').on('change', function(){
      $('.video-settings').slideToggle();
    })

    $('#conv_audio').on('change', function(){
      $('.audio-settings').slideToggle();
    })

    $('#create_thumbnail').on('change', function(){
      $('.thumbnail-settings').slideToggle();
    })
  </script>
  <!-- end scripts -->


</body>
</html>



