
/* Shows notifications for 10 secs if no timeout is given */

function showNotification(thetitle, thecontent, mytimeout) {

    if (isNaN(parseInt(mytimeout))) {
        mytimeout = 10000;
    }

    $.notification(
        {
            title: thetitle,
            content: thecontent,
            timeout: mytimeout,
            showTime: true,
            border: true,
        }
    );
}

function errorNotification(thetitle, thecontent, mytimeout) {

    if (isNaN(parseInt(mytimeout))) {
        mytimeout = 10000;
    }

    $.notification(
        {
            title: thetitle,
            content: thecontent,
            timeout: mytimeout,
            showTime: true,
            border: true,
            error: true,
        }
    );
}

function successNotification(thetitle, thecontent, mytimeout) {

    if (isNaN(parseInt(mytimeout))) {
        mytimeout = 10000;
    }

    $.notification(
        {
            title: thetitle,
            content: thecontent,
            timeout: mytimeout,
            showTime: true,
            border: true,
            success: true,
        }
    );
}
