%include incl_top title=title, topmenu=topmenu
%from app import config
  <div class="container">
    <div class="page-header">
      <h1>Folders <small>view and edit the watched Folders</small></h1>
    </div>

    <div class="col-sm-12 clearfix" style="margin-bottom: 10px">
      <div id="new_profile" class="btn btn-success pull-right">Add New</div>
    </div>
    %if profiles:
    <div id="create_profile" class="not-shown" style="display:none;">
      <div class="well">
        <form class="bs-example form-horizontal" id="myform" method="POST">
          <fieldset>
            <legend>Folder Settings</legend>
            <div class="form-group">
              <label for="path" class="col-lg-2 control-label">Folder Path</label>
              <div class="col-lg-10">
                <input type="text" class="form-control" name="path" id="path" required placeholder="/volume1/Video/xyz">
              </div>
            </div>

            <div class="form-group">
              <label for="interval" class="col-lg-2 control-label">Scan Interval</label>
              <div class="col-lg-10">
                <input type="text" class="form-control" id="interval" name="interval" placeholder="60" required>
              </div>
            </div>

            <div class="form-group">
              <div class="col-lg-offset-2 col-lg-10">
                <div class="delete_finished">
                  <label>
                    <input type="checkbox" name="delete_finished" id="delete_finished" checked> Delete Finished
                  </label>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="profile_id" class="col-lg-2 control-label">Conversion Profile</label>
              <div class="col-lg-10">
                 <select class="form-control" id="profile_id" name="profile_id">
                  %for profile in profiles:
                    <option value="{{profile.id}}">{{profile.name}}</option>
                  %end for
                </select>
              </div>
            </div>

            <div class="form-group">
              <div class="col-lg-10 col-lg-offset-2">
                <button class="btn btn-default">Cancel</button>
                <button id="submit" class="btn btn-success">Submit</button>
              </div>
            </div>

          </fieldset>
        </form>
      </div>
    </div>
    %else:
      <div class="well">Please add a profile first</div>
    %end if
    %if content:
    <div class="table-responsive">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Interval</th>
            <th>Profile</th>
            <th>Options</th>
          </tr>
        </thead>
        %for folder in content:
        <tr data-folder-id="{{folder.id}}">
          <td>{{folder.id}}</td>
          <td>{{folder.name}}</td>
          <td>{{folder.interval}}</td>
          %if folder.profile:
          <td data-profile-id="{{folder.profile_id}}">{{folder.profile.name}}</td>
          %else:
          <td data-profile-id="{{folder.profile_id}}">PROFILE DELETED</td>
          %end if
          <td width="200px">
            <span class="glyphicon glyphicon-refresh"></span>
            <div class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-pencil"></span> edit</div>
            <div class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-trash"></span> delete</div>
          </td>
        </tr>
        %end for
      </table>
    </div>
    %end if
  </div>

  <footer>

  </footer>


  <!-- JavaScript at the bottom for fast page loading -->

  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="/static/js/vendor/jquery-1.10.1.min.js"><\/script>')</script>

  <!-- scripts concatenated and minified via build script -->
  <script src="/static/js/vendor/bootstrap.min.js"></script>
  <script type="text/javascript" src="/static/js/jquery.validate.min.js"></script>
  <script type="text/javascript" src="/static/js/jquery.form.js"></script>
  <script type="text/javascript" src="/static/js/notification.js"></script>
  <script src="/static/js/main.js"></script>
  <script type="text/javascript">

    $('#new_profile').click(function(){
      $('#create_profile').slideDown('fast', function() {
        $(this).removeClass('not-shown')
      });
    })

    // $( "#myform" ).validate({
    //   rules: {
    //     field: {
    //       required: true,
    //       digits: true
    //     }
    //   }
    // });
    $('#submit').click(function(event){
      $("#myform").validate({
        //'debug': true,
        'errorClass': 'has-error',
        'errorElement': 'em',
        highlight: function(element, errorClass) {
          $(element).parents('.form-group').addClass(errorClass);
        },
        rules: {
          interval: {
            required: true,
            digits: true
          }
        },
        submitHandler: function(form) {
          $(form).ajaxSubmit({
            'url':'/folders/add',
            success: function(){
              location.reload()
            }
          });
          //console.log(form)
        }
      });
    })

    $('.btn-danger').click(function(event){
      todelete = $(this).parents('tr').data('folder-id');
      $.post("/folders/delete", {"folder_id": todelete}, function(data){
            console.log(data)

      })
      $(this).parents('tr').fadeOut()
    })

    $('.glyphicon-refresh').click(function(event){
      fid = $(this).parents('tr').data('folder-id');
      $.get("/api/{{config.apikey}}/scan/" + fid, function(data){
        console.log(data)
            x = JSON.parse(data)
            if (x.status == "sucess") {
              successNotification("Sucess", x.message)
            }
      })
    })
  </script>
  <!-- end scripts -->


</body>
</html>



